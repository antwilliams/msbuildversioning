﻿param($installPath, $toolsPath, $package, $project)

# from NuGetPowerTools: https://github.com/davidfowl/NuGetPowerTools
function Resolve-ProjectName {
    param(
        [parameter(ValueFromPipelineByPropertyName = $true)]
        [string[]]$ProjectName
    )
    
    if($ProjectName) {
        $projects = Get-Project $ProjectName
    }
    else {
        # All projects by default
        $projects = Get-Project
    }
    
    $projects
}

function Get-MSBuildProject {
    param(
        [parameter(ValueFromPipelineByPropertyName = $true)]
        [string[]]$ProjectName
    )
    Process {
        (Resolve-ProjectName $ProjectName) | % {
            $path = $_.FullName
            @([Microsoft.Build.Evaluation.ProjectCollection]::GlobalProjectCollection.GetLoadedProjects($path))[0]
        }
    }
}

function Get-RelativePath {
<#
.SYNOPSIS
   Get a path to a file (or folder) relative to another folder
.DESCRIPTION
   Converts the FilePath to a relative path rooted in the specified Folder
.PARAMETER Folder
   The folder to build a relative path from
.PARAMETER FilePath
   The File (or folder) to build a relative path TO
.PARAMETER Resolve
   If true, the file and folder paths must exist
.Example
   Get-RelativePath ~\Documents\WindowsPowerShell\Logs\ ~\Documents\WindowsPowershell\Modules\Logger\log4net.xslt
   
   ..\Modules\Logger\log4net.xslt
   
   Returns a path to log4net.xslt relative to the Logs folder
#>
[CmdletBinding()]
param(
   [Parameter(Mandatory=$true, Position=0)]
   [string]$Folder
, 
   [Parameter(Mandatory=$true, Position=1, ValueFromPipelineByPropertyName=$true)]
   [Alias("FullName")]
   [string]$FilePath
,
   [switch]$Resolve
)
process {
   Write-Verbose "Resolving paths relative to '$Folder'"
   $from = $Folder = split-path $Folder -NoQualifier -Resolve:$Resolve
   $to = $filePath = split-path $filePath -NoQualifier -Resolve:$Resolve

   while($from -and $to -and ($from -ne $to)) {
      if($from.Length -gt $to.Length) {
         $from = split-path $from
      } else {
         $to = split-path $to
      }
   }

   $filepath = $filepath -replace "^"+[regex]::Escape($to)+"\\"
   $from = $Folder
   while($from -and $to -and $from -gt $to ) {
      $from = split-path $from
      $filepath = join-path ".." $filepath
   }
   Write-Output $filepath
}
}

Write-Host "Removing MSBuildVersioning"

$origProj = Get-Project
#Write-Host "Debug $origProj"
$buildProject = $origProj | Get-MSBuildProject

#Write-Host "Debug $buildProject"

	$importToRemove = $buildProject.Xml.Imports | Where-Object { $_.Project.Endswith('MsBuildVersioning.targets') }
    # remove MSBuild targets
    $buildProject.Xml.RemoveChild($importToRemove)


    # figure out which scm we're using and add a proper task
    $path = Get-Location
    $path = [System.IO.PATH]::GetFullPath($path)
    while ($path -ne [System.IO.PATH]::GetPathRoot($path)) {
        Write-Host "Testing $path"
        $type = @((Get-ChildItem $path -filter ".git" -force), (Get-ChildItem $path -filter ".hg" -force), (Get-ChildItem $path -filter ".svn" -force))
        if ($type.Count -gt 0) {
            $scm = $type[0].Name
            Write-Host "Removing BeforeBuild Target for $path $scm"
            
			$target = $buildProject.Xml.Targets | Where-Object {$_.Name -eq "BeforeBuild"} | Select-Object
            switch($scm) {
                ".git" { $task = $buildProject.Xml.Targets | Select-Object -ExpandProperty Children | Where-Object {$_.Name -eq 'GitVersionFile'} | Select-Object }
                ".hg" { $task = $buildProject.Xml.Targets | Select-Object -ExpandProperty Children | Where-Object {$_.Name -eq 'HgVersionFile'} | Select-Object }
                ".svn" { $task = $buildProject.Xml.Targets | Select-Object -ExpandProperty Children | Where-Object {$_.Name -eq 'SvnVersionFile'} | Select-Object }
            }
			$task.Parent.RemoveChild($task)
            break
        }
        $path = [System.IO.PATH]::GetFullPath([System.IO.PATH]::Combine($path, ".."))
    }


$origProj.Save()
